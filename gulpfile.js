var gulp = require('gulp'),
	browserSync = require('browser-sync'),
	reload = browserSync.reload,
    sass = require('gulp-sass'),
    neat = require('node-neat').includePaths,
    autoprefixer = require('gulp-autoprefixer'),
    rename = require('gulp-rename'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    include = require('gulp-include'),
    uglify = require('gulp-uglify'),
    notify = require('gulp-notify');

var paths = {
    html: './src/*.html',
    sass: './src/assets/sass/*.sass',
    js: './src/assets/js/*.js',
    img: './src/assets/img/**/*'
};

gulp.task('serve', function() {
    browserSync({
        server: './src'
    });
});

gulp.task('html', function() {
    return gulp.src(paths.html)
        .pipe(gulp.dest('dist'))
        .pipe(notify({ message: 'HTML task complete' }))
        .pipe(browserSync.reload({stream:true}));
});

gulp.task('sass', function() {
    return gulp.src(paths.sass)
        .pipe(sass({
            indentedSyntax: true,
            includePaths: ['sass'].concat(neat)
        }))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    	.pipe(rename({suffix: '.min'}))
    	.pipe(minifycss())
        .pipe(gulp.dest('src/assets/css'))
    	.pipe(gulp.dest('dist/assets/css'))
    	.pipe(gulp.dest('umbraco/css'))
    	.pipe(notify({ message: 'CSS task complete' }))
    	.pipe(browserSync.reload({stream:true}));
});

gulp.task('img', function() {
    return gulp.src(paths.img)
        .pipe(gulp.dest('dist/assets/img'))
        .pipe(gulp.dest('umbraco/img'))
        .pipe(browserSync.reload({stream:true}));
});

gulp.task('js', function() {
	return gulp.src(paths.js)
	    .pipe(include())
	    .pipe(jshint('.jshintrc'))
	    .pipe(jshint.reporter('default'))
	    .pipe(gulp.dest('src/assets/js'))
	    .pipe(rename({ suffix: '.min' }))
	    .pipe(uglify())
	    .pipe(gulp.dest('dist/assets/js'))
	    .pipe(gulp.dest('umbraco/scripts'))
	    .pipe(notify({ message: 'JS task complete' }))
	    .pipe(browserSync.reload({stream:true}));
});

gulp.task('default', ['html', 'sass', 'img', 'js', 'serve'], function () {
    gulp.watch(paths.html, ['html']);
	gulp.watch(paths.sass, ['sass']);
    gulp.watch(paths.img, ['img']);
    gulp.watch(paths.js, ['js']);
});