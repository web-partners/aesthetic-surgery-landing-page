<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="true" CodeFile="AestheticLandingContact.ascx.cs" Inherits="AestheticLandingContact" %>
<asp:UpdatePanel runat="server">
<ContentTemplate>
<asp:MultiView id="MltContact" runat="server">
<asp:View runat="server">
	<asp:TextBox ID="txtFirstName" CssClass="hidden" runat="server" />
	<asp:TextBox ID="txtName" runat="server" Text="First and Last Name"/>
	<asp:RequiredFieldValidator ValidationGroup="ContactForm" InitialValue="Name" Display="dynamic" CssClass="form-error" ControlToValidate="txtName" runat="server" ErrorMessage="Please enter your name." />
	<asp:TextBox ID="txtPhone" runat="server" Text="Phone Number"/>
	<asp:TextBox ID="txtEmail" runat="server" Text="Email" />
	<asp:RegularExpressionValidator InitialValue="Email" ValidationGroup="ContactForm" Display="dynamic" CssClass="form-error" ControlToValidate="txtEmail" ValidationExpression="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" ErrorMessage="Invalid e-mail." runat="server"/>
	<asp:TextBox ID="txtMessage" runat="server" Text="Comments" TextMode="MultiLine" />
	<asp:RequiredFieldValidator ValidationGroup="ContactForm" InitialValue="Message" Display="dynamic" CssClass="form-error" ControlToValidate="txtMessage" runat="server" ErrorMessage="Please enter a message" />
	<asp:Button ID="ContactSubmitButton" ValidationGroup="ContactForm" runat="server" OnClick="ContactSubmitClick" Text="Send" CssClass="btn btn-primary" />
</asp:View>
<asp:View runat="server">
	<asp:Literal runat="server" id="litThankYou" />
</asp:View>
</asp:MultiView>
</ContentTemplate>
</asp:UpdatePanel>